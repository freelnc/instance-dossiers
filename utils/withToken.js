import axios from 'axios'
import { getCookie } from '/utils/cookies'

const middleware = async () => {
    if (localStorage.token) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.token
    }
    // if (getCookie('token')) {
    //     axios.defaults.headers.common['Authorization'] = 'Bearer ' + getCookie('token')
    // }  
}

export default middleware