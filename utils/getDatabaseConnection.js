const MongoClient = require('mongodb').MongoClient;

let client = null
let database = null
let isConnected = false

export default async () => {
    /* check if we have connection to our databse*/
    if (isConnected) {
      return database
    }
    
    client = new MongoClient(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

    try {
        await client.connect();
        database = client.db('instances');
        isConnected = true;

        return database
    } catch (e) {
        isConnected = false
        console.log(e)
    }
}