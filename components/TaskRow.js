import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  wrapper: {
    height: '48px',
    paddingRight: '24px',
    paddingLeft: '24px',
    backgroundColor: '#EFEFEF',
    borderRadius: '5px',
    position: 'relative',
    marginBottom: '8px',
  },
  status: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: '#6FCF97',
    width: '8px',
    height: '48px',
    borderTopLeftRadius: '5px',
    borderBottomLeftRadius: '5px',
  },
  content: {
    paddingLeft: '16px',
    paddingRight: '24px',
  },
  pretext: {
    color: '#707070'
  },
}));

const handler = ({ task }) => {
  const classes = useStyles()

  return (
    <Grid container className={classes.wrapper} alignItems="center" justify="space-between">
      <div className={classes.status}></div>
      <Grid item xs={4}>
        <Typography>{task.name}</Typography>
      </Grid>
      <Grid item xs={8}>
        <Grid container direction="row" alignItems="center">
          <Grid item xs={3}>
            <Typography><span className={classes.pretext}>Provide</span> {task.service.name}</Typography>
          </Grid>
          <Grid item xs={3}>
            <Typography><span className={classes.pretext}>For</span> {task.client.name}</Typography>
          </Grid>
          <Grid item xs={3}>
            <Typography><span className={classes.pretext}>Due</span> {task.duedate}</Typography>
          </Grid>
          <Grid item xs={3}>
            <Typography><span className={classes.pretext}>By</span> {task.owner.name}</Typography>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

{/* <Grid container className={classes.wrapper} alignItems="center">
      <Grid item className={classes.status}></Grid>
      <Grid item>
        <Grid container justify="space-between" alignItems="center" className={classes.content}>
          <Grid item>
            <Typography>{task.name}</Typography>
          </Grid>
          <Grid item>
            <Grid container>
              <Grid item>
                <Typography><span className={classes.pretext}>Provide</span> {task.service.name}</Typography>
              </Grid>
              <Grid item>
                <Typography><span className={classes.pretext}>For</span> {task.client.name}</Typography>
              </Grid>
              <Grid item>
                <Typography><span className={classes.pretext}>Due</span> {task.duedate}</Typography>
              </Grid>
              <Grid item>
                <Typography><span className={classes.pretext}>By</span> {task.owner.name}</Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid> */}


export default handler