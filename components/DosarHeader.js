import Head from "next/head";
const ulStyle = {
  display: "flex",
  justifyContent: "space-between",
  listStyle: "none",
};
const divStyle = {
  backgroundColor: "#EFEFEF",
  padding: "5px 10px",
  fontWeight: "bold",
  overflow: "hidden",
};
export default function DosarHeader(props) {
  return (
    <div style={divStyle}>
      <ul style={ulStyle}>
        <li>{props.name}</li>
        <li>{"#" + props.id}</li>

        <li>{props.type}</li>
        <li>{props.instance}</li>
        <li>{"Judecător:" + props.judge}</li>
        <li>{props.status}</li>
        <li>
          <button onClick={props.handleClick}> Detalii </button>
        </li>
      </ul>
    </div>
  );
}
