import Head from 'next/head';
import React, { useEffect, useState } from "react";

export default function Agenda({items}) {
    return(
        <table>
            <thead>
            <tr>
              <th>Instanțe judecătorești</th>
              <th>Numărul dosarului</th>
              <th>Judecător</th>
              <th>Data ședinței</th>
              <th>Ora ședinței</th>
              <th>Sala ședinței</th>
              <th>Denumirea dosarului</th>
              <th>Obiectul cauzei</th>
              <th>Tipul dosarului</th>
              <th>Rezultatul ședinței</th>
              <th>Informație</th>
            </tr>
            </thead>
            <tbody>
            {items.map(item => (
             
              <tr key={item.id}>
                {
                  item.map(el =>{
                   
                      if(el.indexOf("pigd")<0)
                      return <td key={el.id}>{el}</td>
                      else return <td key={el.id}><a href={"https://instante.justice.md/ro/"+el}>PDF</a></td>
                  }

                  )
                }
              </tr>
            ))}
            </tbody>
          </table>
    )
}