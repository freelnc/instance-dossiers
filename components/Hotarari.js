import Head from "next/head";
import React from "react";

export default function Hotarari({ items }) {
  return (
    <table>
      <thead>
        <tr>
          <th>Instanțe judecătorești</th>
          <th>Numărul dosarului</th>
          <th>Denumirea dosarului</th>
          <th>Data pronuntarii</th>
          <th>Data inregistrarii</th>
          <th>Data publicarii</th>
          <th>Tipul dosarului</th>
          <th>Tematica dosarului</th>
          <th>Judecător</th>
          <th>Act judecatoresc</th>
        </tr>
      </thead>
      <tbody>
        {items.map((item) => (
          <tr key={item.id}>
            {item.map((el) => {
              if (el.indexOf("pigd") < 0) return <td key={el.id}>{el}</td>;
              else
                return (
                  <td key={el.id}>
                    <a href={"https://instante.justice.md/ro/" + el}>PDF</a>
                  </td>
                );
            })}
          </tr>
        ))}
      </tbody>
    </table>
  );
}
