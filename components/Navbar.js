import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Grid } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { clearSession } from '/redux/sessionSlice'
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import AppleIcon from '@material-ui/icons/Apple';
import NotificationsNoneRoundedIcon from '@material-ui/icons/NotificationsNoneRounded';
import DateRangeRoundedIcon from '@material-ui/icons/DateRangeRounded';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    backgroundColor: '#D1D1D1',
    height: '64px',
    paddingLeft: '24px',
    paddingRight: '24px',
  },
  content: {
    padding: "24px",
  },
  icon: {
    color: '#595959',
    '&:hover': {
      cursor: 'pointer',
      color: '#7818F2'
    }
  },
  avatar: {
    // boxShadow: '1px 1px 1px #00000050',
    marginLeft: '12px',
    '&:hover': {
      cursor: 'pointer',
    }
  },
  brand: {
    color: '#595959'
  }
}));

const handler = () => {
  const classes = useStyles()
  const router = useRouter()
  const dispatch = useDispatch()

  const signOut = () => {
    localStorage.removeItem('token')
    dispatch(clearSession())
    router.push('/auth/login')
  }

  return (
    <Grid container alignItems="center" className={classes.wrapper}>
      <Grid item container justify="space-between" alignItems="center">
        <Grid item>
          <AppleIcon className={classes.brand} />
        </Grid>
        <Grid item>
          <Grid container alignItems="center">
            <Grid item>
              <Button variant="contained" onClick={signOut}>Sign out</Button>
            </Grid>
            <Grid item>
              <IconButton>
                <DateRangeRoundedIcon />
              </IconButton>
            </Grid>
            <Grid item>
              <IconButton variant="contained">
                <NotificationsNoneRoundedIcon />
              </IconButton>
            </Grid>
            <Grid item>
            <Avatar alt="Profile Picture" className={classes.avatar} src="/demo-avatar.png" />
              {/* <Avatar alt="Profile Picture" className={classes.avatar} src="https://i.pinimg.com/474x/da/7a/cc/da7acc751f22f6931a3c0e97ee143f7f.jpg" /> */}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default handler