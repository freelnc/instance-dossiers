import Head from "next/head";
import React, { useEffect, useState } from "react";
import DosarHeader from "../components/DosarHeader";
import DosarBody from "../components/DosarBody";

export default function Dosar(props) {
  const [items, setItems] = useState({ ...props.data });
  const [open, setOpen] = useState(false);
  React.useEffect(() => {
    setItems(props.data);
  }, [props.data]);

  return (
    <div className="results">
      {/* <button handleClick={() => setOpen(!open)}> Detalii </button> */}
      <DosarHeader
        id={items.id}
        name={items.name}
        type={items.type}
        instance={items.instance}
        judge={items.judge}
        status={items.status}
        handleClick={() => setOpen(!open)}
      />
      {open && <DosarBody timeline={items.timeline} />}
    </div>
  );
}
