import Head from "next/head"
import DosarBodyElem from "./DosarBodyElem";


export default function DosarBody({timeline}){
    // for(let i=0;i<timeline.length;i++){
    //     timeline[i].date = Date.parse(timeline[i].date);
    // }
    timeline.forEach(function (el) {
        let res = el.date.split("-");
        let date;
        if(el.type == "meeting"){
            date = new Date(res[2],res[1]-1,res[0])
        }
        else if(el.type == "decision" || el.type == "closing" || el.type == "status_change"){
            date = new Date(res[0],res[1]-1,res[2])
        }
        
            var curr_date = date.getDate();
            var curr_month = date.getMonth()+1; 
            var curr_year = date.getFullYear();
            if(curr_month<10) curr_month="0"+curr_month;
            el.date = curr_month +"-"+curr_date+"-"+curr_year;
            
    })
    timeline.sort((a,b) => Date.parse(a.date) <= Date.parse(b.date) ? 1 : -1)
    let listElems=[];
    //  let listElems = timeline.map((elem,index) => {
    //     return <DosarBodyElem item={elem} key={index}/>
    //  })
    if(timeline.length >= 4){
        for(let i=0;i<4;i++){
         listElems[i] = <DosarBodyElem item={timeline[i]} key={i}/>
     }
    }
    else{
        for(let i=0;i<timeline.length;i++){
            listElems[i] = <DosarBodyElem item={timeline[i]} key={i}/>
        }
    }
     
    return (
        <div className="results-block">
        {listElems}
        </div>
    )
}