import Head from "next/head"


export default function DosarBodyElem({item}){

    if(item.type == "meeting"){
        return <div className="results-item"><div className="circle"></div><strong>Ședință</strong> pe data de <strong>{item.date}</strong> la ora <strong>{item.details.time}</strong>, la <strong>{item.details.place}</strong></div>
    }
    else if(item.type == "decision"){
        return  (<div className="results-item"><div className="circle"></div><strong>Hotărâre</strong> publicată pe  <strong>{item.date}</strong>  și urmează a fi pronunțată pe  <strong>{item.details.pronunciation_date}</strong> <a href={"https://instante.justice.md/ro/"+item.details.doc_link} target="_blank">PDF</a></div>)
    }
    else if(item.type == "closing"){
        return  (<div className="results-item"><div className="circle"></div><strong>încheiere</strong> înregistrată pe <strong>{item.date}</strong> și publicată pe  <strong>{item.details.pronunciation_date}</strong> <a href={"https://instante.justice.md/ro/"+item.details.doc_link} target="_blank">PDF</a></div>)
    }
    else if(item.type == "status_change"){
        return  (<div className="results-item"><div className="circle"></div><strong>{item.details.status}</strong> </div>)
    }
    
}