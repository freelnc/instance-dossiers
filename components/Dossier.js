import { useRef, useEffect, useState } from 'react'
import clsx from 'clsx'
import styles from '../styles/Dossier.module.css'

export default function component({ dossier }) {
  const [isCollapsed, setIsCollapsed] = useState(false)
  const el = useRef()
  const [elheight, setElHeight] = useState(0);

  useEffect(
    () => {
      setElHeight(el.current.scrollHeight);
    },
    [elheight]
  );

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <strong>{dossier.name}</strong>
        <span>{dossier.id}</span>
        <span>{dossier.type}</span>
        <span>{dossier.instance}</span>
        <span>{dossier.judge}</span>
        <div className={styles.status}>{dossier.status != "" ? dossier.status : 'Examinare'}</div>
        <div className={isCollapsed ? clsx(styles.btnCollapse, styles.active) : styles.btnCollapse} onClick={() => setIsCollapsed(!isCollapsed)}>
          <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2.49163 6.15007C2.08329 6.55841 2.08329 7.21674 2.49163 7.62507L9.41663 14.5501C9.74163 14.8751 10.2666 14.8751 10.5916 14.5501L17.5166 7.62507C17.925 7.21674 17.925 6.55841 17.5166 6.15007C17.1083 5.74174 16.45 5.74174 16.0416 6.15007L9.99996 12.1834L3.95829 6.14174C3.55829 5.74174 2.89163 5.74174 2.49163 6.15007V6.15007Z" fill="#424242"/>
          </svg>
        </div>
      </div>
      <div ref={el} className={styles.body} style={{maxHeight: isCollapsed ? 0 : elheight}}>
        {dossier.timeline ? dossier.timeline.map(item => (<DossierTimelineItem key={item.type + item.date} {...item} />)) : ''}
      </div>
    </div>  
  )
}

function DossierTimelineItem({ type, date, details }) {
  if (type == 'meeting') {
    return (
      <div className={styles.dossierTimelineItem}>
        <strong>Ședință</strong> pe data de <strong>{details.date}</strong> la ora <strong>{details.time}</strong>, la <strong>{details.place}</strong>
        {details.result != "" ? <div className={styles.dossierTimelineItemInfo}>{details.result}</div> : ''}
      </div>
    )
  } else if (type == 'decision') {
    return (
      <div className={styles.dossierTimelineItem}>
        <strong>Hotărîre</strong> publicată pe data de <strong>{details.publicationDate}</strong> și urmează a fi pronunțată pe data de <strong>{details.pronunciationDate}</strong> <a href={details.docLink}>PDF</a>
      </div>
    )
  } else if (type == 'closing') {
    return (
      <div className={styles.dossierTimelineItem}>
        <strong>Încheiere</strong> înregistrată pe data de <strong>{details.registrationDate}</strong> și publicată pe data de <strong>{details.publicationDate}</strong> <a href={details.docLink}>PDF</a>
      </div>
    )
  } else if (type == 'status') {
    return (
      <div className={styles.dossierTimelineItem}>
        {/* Statut modificat în <strong>{details.status}</strong> pe data de <strong>{date}</strong> */}
        <strong>{details.status}</strong> pe data de <strong>{date}</strong>
      </div>
    )
  }
  
  return <></>
}