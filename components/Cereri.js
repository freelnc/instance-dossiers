import Head from "next/head";
import React from "react";

export default function Cereri({ items }) {
  return (
    <table>
      <thead>
        <tr>
          <th>Instanțe judecătorești</th>
          <th>Numărul dosarului</th>
          <th>Numărul de referință a dosarului</th>
          <th>Data înregistrării</th>
          <th>Statutul dosarului</th>
          <th>Tipul dosarului</th>
          <th>Denumirea dosarului </th>
          <th>Informație</th>
        </tr>
      </thead>
      <tbody>
        {items.map((item) => (
          <tr key={item.id}>
            {item.map((el) => {
              if (el.indexOf("pigd") < 0) return <td key={el.id}>{el}</td>;
              else
                return (
                  <td key={el.id}>
                    <a href={"https://instante.justice.md/ro/" + el}>PDF</a>
                  </td>
                );
            })}
          </tr>
        ))}
      </tbody>
    </table>
  );
}
