import { Grid } from "@material-ui/core"
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    minHeight: '100vh',
  },
  root: {
    position: 'relative',
  },
  bottom: {
    color: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
  },
  top: {
    color: '#1a90ff',
    animationDuration: '1500ms',
    position: 'absolute',
    left: 0,
  },
  circle: {
    strokeLinecap: 'round',
  },
}));

const handler = () => {
  const classes = useStyles()
  const size = 100;

  return (
    <Grid container justify="center" alignItems="center" className={classes.wrapper}>
      <Grid item className={classes.root}>
        <CircularProgress
          variant="determinate"
          className={classes.bottom}
          size={size}
          thickness={4}
          value={100}
        />
        <CircularProgress
          variant="indeterminate"
          // disableShrink
          className={classes.top}
          classes={{
            circle: classes.circle,
          }}
          size={size}
          thickness={4}
        />
      </Grid>
    </Grid>
  )
}

export default handler