import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const initialState = {
  status: 'idle',
  checked: false,
  active: false,
}

export const getSession = createAsyncThunk('session/check', async () => {
  const response = await axios.get('/session/check')
  return response.data
})

const slice = createSlice({
  name: 'session',
  initialState,
  reducers: {
    clearSession(state, action) {
      state.status = 'idle'
      state.active = false
    },
    activateSession(state, action) {
      state.status = 'idle'
      state.checked = true
      state.active = true
    }
  },
  extraReducers: {
    [getSession.pending]: (state, action) => {
      state.status = 'pending'
    },
    [getSession.fulfilled]: (state, action) => {
      state.status = 'fulfilled'
      state.active = action.payload.ok
      state.checked = true
    },
    [getSession.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.payload
    },
  },
})

export const { clearSession, activateSession } = slice.actions

export default slice.reducer