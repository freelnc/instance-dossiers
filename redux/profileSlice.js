import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const initialState = {
  status: 'pending',
  authorized: false,
  profile: {}
}

export const authenticateProfile = createAsyncThunk('profile/authenticate', async () => {
  const response = await axios.get('/profile/authenticate')
  return response.data
})

const slice = createSlice({
  name: 'profile',
  initialState,
  extraReducers: {
    [authenticateProfile.pending]: (state, action) => {
      state.status = 'pending'
    },
    [authenticateProfile.fulfilled]: (state, action) => {
      state.status = 'fulfilled'
      state.authorized = action.payload.ok
      state.data = action.payload
    },
    [authenticateProfile.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.payload
    },
  },
})

// export const { clearCustomer, setFilter } = customersSlice.actions

export default slice.reducer