import Head from "next/head";
import React, { useEffect, useState } from "react";
import Form from "../components/Form";
import Dosar from "../components/Dosar";
import { render } from "react-dom";

export default function MyApp() {
  const [error, setError] = useState(null);

  const [data, setData] = useState([]);
  const [data2, setData2] = useState([]);
  const [isLoaded, setIsLoaded] = useState(null);
  const [loader, setLoader] = useState("");

  const handleChange = (e) => {
    setIsLoaded(false);
    setLoader("loading...");
    setData([]);
    setData2([]);
    e.preventDefault();
    if (e.target.dosar.value != "") {
      fetch("/api/agenda?value=" + e.target.dosar.value)
        .then((res) => res.json())
        .then((result) => {
          setData(result);
          setIsLoaded(true);
        });
    }
    if (e.target.dosar2.value != "") {
      fetch("/api/agenda?value=" + e.target.dosar2.value)
        .then((res) => res.json())
        .then((res) => {
          setData2(res);
        });
    }
    if (e.target.dosar3.value != "") {
      fetch("/api/contestare3?value=" + e.target.dosar3.value)
        .then((res) => res.json())
        .then((res) => {
          console.log(1);
        });
    }
  };

  return (
    <>
      <Form handleChange={handleChange} />
      {isLoaded ? <Dosar data={data} key={1} /> : loader}
      {data2.id != undefined && <Dosar data={data2} key={2} />}
    </>
  );
}
