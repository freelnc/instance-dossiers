import { providers, signIn, getCsrfToken } from "next-auth/client";
import { useState } from 'react'
import axios from 'axios'
import { Button, Grid, Typography, FormControl, InputLabel, OutlinedInput, FormHelperText } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import CircularProgress from '@material-ui/core/CircularProgress';

axios.defaults.validateStatus = status => true

const useStyles = makeStyles((theme) => ({
  heading: {
    marginBottom: '48px'
  },
  wrapper: {
    minHeight: '100vh',
  },
  authBox: {
    maxWidth: '350px',
    width: '90vw',
    height: '300px',
    marginBottom: '100px'
  },
  button: {
    marginTop: '16px',
    display: 'flex!important'
  },
  progress: {
    color: '#ffffff'
  },
  password: {
    marginTop: '8px'
  }
}));

export default function SignIn({ csrfToken }) {
  const classes = useStyles()
  const [email, setEmail] = useState('')
  const [emailErrorMessage, setEmailErrorMessage] = useState('')
  const [password, setPassword] = useState('')
  const [passwordErrorMessage, setPasswordErrorMessage] = useState('')
  const [isPending, setIsPending] = useState(false)
  
  const handleChangeEmail = e => {
    if (emailErrorMessage != '') {
      setEmailErrorMessage('')
    }

    setEmail(e.target.value)
  }

  const handleChangePassword = e => {
    if (passwordErrorMessage != '') {
      setPasswordErrorMessage('')
    }

    setPassword(e.target.value)
  }

  const handleSubmit = async e => {
    e.preventDefault()
    
    if (email == '') {
      setEmailErrorMessage('Completează cu adresa de Email')
      return
    }

    if (password == '') {
      setPasswordErrorMessage('Completează cu parola')
      return
    }

    setIsPending(true)
    setEmailErrorMessage('')
    setPasswordErrorMessage('')

    const resp = await axios.post('/api/auth/callback/credentials', { email: email, password: password, csrfToken: csrfToken })

    if (resp.status != 200) {
      setEmailErrorMessage('Problema pe partea de server')
      setIsPending(false)
      return
    }
    
    if (resp.data.ok == false) {
      setEmailErrorMessage(resp.data.message)
      setIsPending(false)
      return
    }

    if (resp.data.userExists == false) {
      setEmailErrorMessage(resp.data.message)
      setIsPending(false)
      return
    }

    if (resp.data.message) {
      setPasswordErrorMessage(resp.data.message)
      setIsPending(false)
      return
    }

    if (emailErrorMessage != '') {
      setEmailErrorMessage('')
    }

    if (passwordErrorMessage != '') {
      setPasswordErrorMessage('')
    }

    console.log('Success')
    setIsPending(false)
  }

  return (
    <Grid container justify="center" alignItems="center" className={classes.wrapper}>
      <Grid item>
          <div>
            <form className={classes.authBox} onSubmit={handleSubmit} autoComplete='off'>
              <input name='csrfToken' type='hidden' defaultValue={csrfToken} />
              <Typography variant="h4" component="h1" gutterBottom className={classes.heading}>Autentificare</Typography>
              <FormControl error={emailErrorMessage ? true : false} variant="outlined" size="small" fullWidth>
                <InputLabel>Adresa de Email</InputLabel>
                <OutlinedInput
                  name="username"
                  value={email}
                  label="Adresa de Email"
                  onChange={handleChangeEmail}  
                />
                <FormHelperText>{emailErrorMessage}</FormHelperText>
              </FormControl>
              <FormControl
                variant="outlined"
                size="small"
                error={passwordErrorMessage ? true : false}
                fullWidth
                className={classes.password}
               >
                <InputLabel>Parola</InputLabel>
                <OutlinedInput
                  name="name"
                  type="password"
                  value={password}
                  label="Parola"
                  onChange={handleChangePassword}
                />
                <FormHelperText>{passwordErrorMessage}</FormHelperText>
              </FormControl>
              <Button variant="contained" color="primary" className={classes.button} type="submit" fullWidth>
                { isPending ? <CircularProgress className={classes.progress} size={24} /> : <ArrowForwardIcon /> }
              </Button>
            </form>
          </div>
      </Grid>
    </Grid>
  );
}

export async function getServerSideProps(context) {
  return {
    props: {
      csrfToken: await getCsrfToken(context)
    }
  }
}