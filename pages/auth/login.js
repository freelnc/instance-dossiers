import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import axios from 'axios'
import { Button, Grid, Typography, FormControl, InputLabel, OutlinedInput, FormHelperText } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import CircularProgress from '@material-ui/core/CircularProgress';
import { setCookie } from '/utils/cookies'
import { useDispatch, useSelector } from 'react-redux'
import { getSession, activateSession } from '/redux/sessionSlice'
import withToken from '/utils/withToken'
import Loader from '/components/Loader'

axios.defaults.validateStatus = status => true

const useStyles = makeStyles((theme) => ({
  heading: {
    marginBottom: '48px'
  },
  wrapper: {
    minHeight: '100vh',
  },
  authBox: {
    maxWidth: '350px',
    width: '90vw',
    height: '300px',
    marginBottom: '100px'
  },
  button: {
    marginTop: '16px',
    display: 'flex!important'
  },
  progress: {
    color: '#ffffff'
  },
  password: {
    marginTop: '8px'
  }
}));

export default function page() {
  const classes = useStyles()
  const router = useRouter()
  const dispatch = useDispatch()
  const session = useSelector(state => state.session)
  const [email, setEmail] = useState('')
  const [emailErrorMessage, setEmailErrorMessage] = useState('')
  const [password, setPassword] = useState('')
  const [passwordErrorMessage, setPasswordErrorMessage] = useState('')
  const [isPending, setIsPending] = useState(false)

  useEffect(() => {
    withToken()

    if (session.checked == false) {
      dispatch(getSession())
    } else if (session.active) {
      router.push('/cabinet')
    }
  }, [dispatch])

  if (session.status == 'pending' || session.checked == false) {
    return <Loader />
  }

  if (session.status == 'fulfilled' && session.active) {
    router.push('/cabinet')
    return <></>
  }

  const handleChangeEmail = e => {
    if (emailErrorMessage != '') {
      setEmailErrorMessage('')
    }

    setEmail(e.target.value)
  }

  const handleChangePassword = e => {
    if (passwordErrorMessage != '') {
      setPasswordErrorMessage('')
    }

    setPassword(e.target.value)
  }

  const handleSubmit = async e => {
    e.preventDefault()
    
    if (email == '') {
      setEmailErrorMessage('Completează cu adresa de Email')
      return
    }

    if (password == '') {
      setPasswordErrorMessage('Completează cu parola')
      return
    }

    setIsPending(true)
    setEmailErrorMessage('')
    setPasswordErrorMessage('')

    const resp = await axios.post('/auth/login', { email: email, password: password })

    if (resp.status != 200) {
      setEmailErrorMessage('Problema pe partea de server')
      setIsPending(false)
      return
    }
    
    if (resp.data.ok == false) {
      setEmailErrorMessage(resp.data.message)
      setIsPending(false)
      return
    }

    if (resp.data.userExists == false) {
      setEmailErrorMessage(resp.data.message)
      setIsPending(false)
      return
    }

    if (resp.data.message) {
      setPasswordErrorMessage(resp.data.message)
      setIsPending(false)
      return
    }

    if (emailErrorMessage != '') {
      setEmailErrorMessage('')
    }

    if (passwordErrorMessage != '') {
      setPasswordErrorMessage('')
    }

    localStorage.setItem('token', resp.data.token)
    dispatch(activateSession())
    router.push('/cabinet')
  }

  return (
    <Grid container justify="center" alignItems="center" className={classes.wrapper}>
      <Grid item>
          <div>
            <form className={classes.authBox} onSubmit={handleSubmit} autoComplete='off'>
              <Typography variant="h4" component="h1" gutterBottom className={classes.heading}>Autentificare</Typography>
              <FormControl error={emailErrorMessage ? true : false} variant="outlined" size="small" fullWidth>
                <InputLabel>Adresa de Email</InputLabel>
                <OutlinedInput
                  value={email}
                  label="Adresa de Email"
                  onChange={handleChangeEmail}  
                />
                <FormHelperText>{emailErrorMessage}</FormHelperText>
              </FormControl>
              <FormControl
                variant="outlined"
                size="small"
                error={passwordErrorMessage ? true : false}
                fullWidth
                className={classes.password}
               >
                <InputLabel>Parola</InputLabel>
                <OutlinedInput
                  type="password"
                  value={password}
                  label="Parola"
                  onChange={handleChangePassword}
                />
                <FormHelperText>{passwordErrorMessage}</FormHelperText>
              </FormControl>
              <Button variant="contained" color="primary" className={classes.button} type="submit" fullWidth>
                { isPending ? <CircularProgress className={classes.progress} size={24} /> : <ArrowForwardIcon /> }
              </Button>
            </form>
          </div>
      </Grid>
    </Grid>
  )
}