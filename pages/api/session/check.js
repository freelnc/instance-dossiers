import withProtect from '/middleware/withProtect'
import getDatabaseConnection from '/utils/getDatabaseConnection'

const handler = (req, res) => {
  const db = getDatabaseConnection()
  // const users = db.collection('users')
  // const { email, password } = req.body

  // const user = await users.findOne({ email: email })

  // console.log(user)
  res.status(200).json({ ok: true })

}

export default withProtect(handler)