const MongoClient = require('mongodb').MongoClient;

const uri = "mongodb+srv://root:saniok2000@instance.q6kpj.mongodb.net/instances?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });



export default async (req, res) => {
  try {
    const { email } = req.body

    await client.connect();
    const database = client.db('instances');
    const users = database.collection('users');
    const user = await users.findOne({ email: email })

    if (user) {
      res.status(200).json({ ok: true, userExists: true })
    } else {
      res.status(200).json({ ok: true, userExists: false, message: 'Utilizător inexistent' })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({errorMessage: 'Unknown'})
  } finally {
    // Ensures that the client will close when you finish/error
    console.log('finally')
    // await client.close();
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}