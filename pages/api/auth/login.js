import getDatabaseConnection from '../../../utils/getDatabaseConnection'
import jwt from 'jsonwebtoken'
import withProtect from '../../../middleware/withProtect'
require("dotenv").config();

const handler = async (req, res) => {
  const db = await getDatabaseConnection()
  const users = db.collection('users')
  const { email, password } = req.body

  const user = await users.findOne({ email: email })

  if (user == null) {
    res.status(200).json({ ok: true, userExists: false, message: 'Utilizător inexistent' })
    return
  }

  if (user.password != password) {
    res.status(200).json({ ok: true, userExists: true, message: 'Parolă incorectă' })
    return
  }

  var token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET)

  res.status(200).json({ ok: true, userExists: true, token: token })
}

export default handler