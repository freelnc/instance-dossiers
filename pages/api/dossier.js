const asyn = require('async');
const axios = require("axios");

axios.defaults.validateStatus = status => true; // default

export default async (req, res) =>  {
  const dossierId = req.query.id;

  asyn.parallel({
    meetings: async () => await getRows(1, `https://instante.justice.md/ro/agenda-sedintelor?Instance=All&Denumire_dosar=&Numarul_cazului=${dossierId}&Obiectul_cauzei=&Tipul_dosarului=All`),
    decisions: async () => await getRows(2, `https://instante.justice.md/ro/hotaririle-instantei?Instance=All&Numarul_dosarului=${dossierId}&Denumirea_dosarului=&date=&Tematica_dosarului=&Tipul_dosarului=All`),
    closings: async () => await getRows(3, `https://instante.justice.md/ro/incheierile-instantei?Instance=All&Denumirea_dosarului=&Numarul_dosarului=${dossierId}&date=&Tematica_dosarului=&Tipul_dosarului=All`),
    statuses: async () => await getRows(4, `https://instante.justice.md/ro/cereri-si-doasare-pendite?Instance=All&Denumirea_dosarului=&Numarul_dosarului=${dossierId}&Statutul_dosarului=&date=&Tipul_dosarului=All`)
  }, (err, results) => {
    let processedAtLeastOneRow = false
    let dossier = {
      status: false,
      timeline: []
    }
    
    if (results.meetings) {
      for (const meeting of results.meetings) {
        if (processedAtLeastOneRow == false) {
          dossier = {
            ...dossier,
            id: meeting[1],
            judge: meeting[2],
            name: meeting[6],
            type: meeting[8],
            instance: meeting[0]
          }
        }

        dossier.timeline.push({
          type: 'meeting',
          date: getDate(meeting[3]),
          details: {
            date: getDate(meeting[3]),
            time: meeting[4],
            place: meeting[5],
            result: meeting[9]
          }
        })

        processedAtLeastOneRow = true
      }
    }

    if (results.decisions) {
      for (const decision of results.decisions) {
        if (processedAtLeastOneRow == false) {
          dossier = {
            ...dossier,
            id: decision[1],
            judge: decision[8],
            name: decision[2],
            type: decision[6],
            instance: decision[0]
          }
        }

        dossier.timeline.push({
          type: 'decision',
          date: getDate(decision[3]),
          details: {
            publicationDate: getDate(decision[5]),
            pronunciationDate: getDate(decision[3]),
            docLink: getLink(decision[9])
          }
        })

        processedAtLeastOneRow = true
      }
    }

    if (results.closings) {
      for (const closing of results.closings) {
        if (processedAtLeastOneRow == false) {
          dossier = {
            ...dossier,
            id: closing[1],
            judge: closing[7],
            name: closing[2],
            type: closing[5],
            instance: closing[0]
          }
        }

        dossier.timeline.push({
          type: 'closing',
          date: getDate(closing[3]),
          details: {
            registrationDate: getDate(closing[3]),
            publicationDate: getDate(closing[4]),
            docLink: getLink(closing[8])
          }
        })

        processedAtLeastOneRow = true
      }
    }

    if (results.statuses) {
      let maxDate = '0000-00-00'

      for (const status of results.statuses) {
        if (processedAtLeastOneRow == false) {
          dossier = {
            ...dossier,
            id: status[1],
            judge: false,
            name: status[6],
            type: status[5],
            instance: status[0]
          }
        }

        dossier.timeline.push({
          type: 'status',
          date: getDate(status[3]),
          details: {
            status: status[4]
          }
        })

        if (maxDate <= getDate(status[3])) {
          dossier.status = status[4]
        }

        processedAtLeastOneRow = true
      }
    }
    
    dossier.timeline.sort((a, b) => a.date < b.date ? 1 : -1)

    res.status(200).json(dossier);
  })
}

const getRows = async (id, url) => {
  let result = []
  let resp = await axios.get(url)
  
  if (resp.status != 200) {
    console.log(id + " => " + resp.status)
    return false
  }
  
  let html = resp.data

  html = html.split(/<tbody[^>]*>\s*<tr[^>]*>\s*<td[^>]*>\s*/)[1]
  
  if (html == undefined) {
    return []
  }

  html = html.split(/\s*<\/td>\s*<\/tr>\s*<\/tbody>/)[0]

  let rows = html.split(/\s*<\/td>\s*<\/tr>\s*<tr[^>]*>\s*<td[^>]*>\s*/)
  
  for (const row of rows) {
    result.push(row.split(/\s*<\/td>\s*<td[^>]*>\s*/))
  }

  return result
}

const getLink = html => {
  let res = /<a[^>]*href="([^>"]*)"[^>]*>/.exec(html)
  if (res) {
    return `https://instante.justice.md/ro/${res[1]}`
  }

  return html
}

const getDate = string => {
  let res = string.split("-")
  let date = new Date(res[2], parseInt(res[1]) - 1, res[0])

  if (isNaN(date.getTime()) || date.getTime() < 0) {
    date = new Date(res[0], parseInt(res[1]) - 1, res[2])
  }

  let day = date.getDate()
  let month = date.getMonth() + 1
  let year = date.getFullYear()
  
  if (month < 10) {
    month = `0${month}`
  }

  if (day < 10) {
    day = `0${day}`
  }

  return `${year}-${month}-${day}`
}