import Hotarari from "../../components/Hotarari";
import parallel from "async/parallel";
const asyn = require("async");
const axios = require("axios");
const cheerio = require("cheerio");

axios.defaults.validateStatus = function (status) {
  return true; // default
};

export default function handler(req, res) {
  // console.log(req)
  var result = [];
  const { value } = req.query;
  // axios({
  //   method: "post",
  //   url: "http://jurisprudenta.csj.md/grid_lista_dosare.php",
  //   data: {
  //     _search: true,
  //     nd: Date.now(),
  //     rows: 50,
  //     page: 1,
  //     sord: "desc",
  //     filters: {
  //       groupOp: "AND",
  //       rules: [{ field: "nr_inregistrare", op: "cn", data: value }],
  //     },
  //   },
  // }).then((result) => {
  //   console.log(result.data.rows);
  // });
  var obj = {};
  obj = {
    id: "",
    name: "",
    type: "",
    instance: "",
    judge: "",
    status: "",
    timeline: [],
  };
  const agenda = async () => {
    let error = false;
    const resp = await axios
      .get(
        "https://instante.justice.md/ro/agenda-sedintelor?Instance=All&Denumire_dosar=&Numarul_cazului=" +
          value +
          "&Obiectul_cauzei=&Tipul_dosarului=All"
      )
      .catch((err) => {
        error = true;
      });
    // console.log(resp.status)

    if (resp.status != 200) {
      return resp.status;
    }

    const $ = cheerio.load(resp.data, { decodeEntities: false });

    let results = $("table.views-table>tbody tr");

    if (results.length != 0) {
      if (obj.id == "" || obj.judge == "") {
        let first = $("table.views-table>tbody tr:first-child");
        obj.id = $(first)
          .children(".views-field-Numarul-cazului")
          .html()
          .trim();
        obj.name = $(first)
          .children(".views-field-Denumire-dosar")
          .html()
          .trim();
        obj.type = $(first)
          .children(".views-field-Tipul-dosarului")
          .html()
          .trim();
        obj.instance = $(first)
          .children(".views-field-solr-document")
          .html()
          .trim();
        obj.judge = $(first)
          .children(".views-field-solr-document-2")
          .html()
          .trim();
      }

      results.each(function (index, el) {
        let row = {
          type: "meeting",
          date: $(el).children(".views-field-solr-document-3").html().trim(),
          details: {
            date: $(el).children(".views-field-solr-document-3").html().trim(),
            place: $(el).children(".views-field-solr-document-5").html().trim(),
            time: $(el).children(".views-field-solr-document-4").html().trim(),
            result: $(el)
              .children(".views-field-Rezultatul-sedintei")
              .html()
              .trim(),
          },
        };

        obj.timeline.push(row);
      });
    }

    return 1;
  };

  const hotarari = async () => {
    let error = false;
    const resp = await axios
      .get(
        "https://instante.justice.md/ro/hotaririle-instantei?Instance=All&Numarul_dosarului=" +
          value +
          "&Denumirea_dosarului=&date=&Tematica_dosarului=&Tipul_dosarului=All"
      )
      .catch((err) => {
        error = true;
      });
    // console.log(resp.status)
    if (resp.status != 200) {
      return resp.status;
    }
    const $ = cheerio.load(resp.data, { decodeEntities: false });

    let results = $("table.views-table>tbody tr");
    if (results.length != 0) {
      if (obj.id == "") {
        let first = $("table.views-table>tbody tr:first-child");
        obj.id = $(first)
          .children(".views-field-Numarul-dosarului")
          .html()
          .trim();
        obj.name = $(first)
          .children(".views-field-Denumirea-dosarului")
          .html()
          .trim();
        obj.type = $(first)
          .children(".views-field-Tipul-dosarului")
          .html()
          .trim();
        obj.instance = $(first)
          .children(".views-field-solr-document-5")
          .html()
          .trim();
        obj.judge = $(first)
          .children(".views-field-solr-document-3")
          .html()
          .trim();
      }
      results.each(function (index, el) {
        let row = {
          type: "decision",
          date: $(el).children(".views-field-solr-document").html().trim(),
          details: {
            publication_date: $(el)
              .children(".views-field-solr-document-2")
              .html()
              .trim(),
            pronunciation_date: $(el)
              .children(".views-field-solr-document-1")
              .html()
              .trim(),
            doc_link: $(el)
              .children(".views-field-nothing")
              .children("a")
              .attr("href"),
          },
        };

        obj.timeline.push(row);
      });
    }
    return 1;
  };
  const incheieri = async () => {
    let error = false;
    const resp = await axios
      .get(
        "https://instante.justice.md/ro/incheierile-instantei?Instance=All&Denumirea_dosarului=&Numarul_dosarului=" +
          value +
          "&date=&Tematica_dosarului=&Tipul_dosarului=All"
      )
      .catch((err) => {
        console.log(err);
        error = true;
      });

    //  console.log(resp.status)

    if (resp.status != 200) {
      return resp.status;
    }
    const $ = cheerio.load(resp.data, { decodeEntities: false });
    let results = $("table.views-table>tbody tr");
    if (results.length != 0) {
      if (obj.id == "") {
        let first = $("table.views-table>tbody tr:first-child");
        obj.id = $(first)
          .children(".views-field-Numarul-dosarului")
          .html()
          .trim();
        obj.name = $(first)
          .children(".views-field-Denumirea-dosarului")
          .html()
          .trim();
        obj.type = $(first)
          .children(".views-field-Tipul-dosarului")
          .html()
          .trim();
        obj.instance = $(first)
          .children(".views-field-solr-document-4")
          .html()
          .trim();
        obj.judge = $(first)
          .children(".views-field-solr-document-2")
          .html()
          .trim();
      }
      results.each(function (index, el) {
        let row = {
          type: "closing",
          date: $(el).children(".views-field-solr-document").html().trim(),
          details: {
            registration_date: $(el)
              .children(".views-field-solr-document")
              .html()
              .trim(),
            publication_date: $(el)
              .children(".views-field-solr-document-1")
              .html()
              .trim(),
            doc_link: $(el)
              .children(".views-field-nothing")
              .children("a")
              .attr("href"),
          },
        };

        obj.timeline.push(row);
      });
    }
    return 1;
  };
  const status_change = async () => {
    let error = false;
    const resp = await axios
      .get(
        `https://instante.justice.md/ro/cereri-si-doasare-pendite?Instance=All&Denumirea_dosarului=&Numarul_dosarului=${value}&Statutul_dosarului=&date=&Tipul_dosarului=All`
      )
      .catch((err) => {
        error = true;
      });
    //  console.log(resp.status)
    if (resp.status != 200) {
      return resp.status;
    }

    const $ = cheerio.load(resp.data, { decodeEntities: false });
    let results = $("table.views-table>tbody tr");
    if (results.length != 0) {
      if (obj.id == "") {
        let first = $("table.views-table>tbody tr:first-child");
        obj.id = $(first)
          .children(".views-field-Numarul-dosarului")
          .html()
          .trim();
        obj.name = $(first)
          .children(".views-field-Denumirea-dosarului")
          .html()
          .trim();
        obj.type = $(first)
          .children(".views-field-Tipul-dosarului")
          .html()
          .trim();
        obj.instance = $(first)
          .children(".views-field-solr-document-2")
          .html()
          .trim();
        obj.status = $(first)
          .children(".views-field-Statutul-dosarului")
          .html()
          .trim();
        obj.judge = "";
      }
      results.each(function (index, el) {
        let row = {
          type: "status_change",
          date: $(el).children(".views-field-solr-document").html().trim(),
          details: {
            status: $(el)
              .children(".views-field-Statutul-dosarului")
              .html()
              .trim(),
          },
        };
        if (index == 0) {
          obj.status = $(el)
            .children(".views-field-Statutul-dosarului")
            .html()
            .trim();
        }
        obj.timeline.push(row);
      });
    }
    return 1;
  };
  // agenda().then(function(){
  //   hotarari().then(function(){
  //     incheieri().then(function(){
  //       status_change().then(function(){
  //         console.log(JSON.stringify(obj, null, 2))
  //       })

  //     })

  //   })

  // })
  asyn.parallel(
    {
      one: agenda,
      two: hotarari,
      three: incheieri,
      four: status_change,
    },
    function (err, results) {
      console.log(results);
      console.log(JSON.stringify(obj, null, 2));
      res.status(200).json(obj);
    }
  );

  // (async () => {
  //   console.log(1234);

  //   let resp1
  //   let resp2
  //   let resp3
  //   let resp4

  //   await (async () => {
  //     resp1 = agenda()
  //     resp2 = hotarari()
  //     resp3 = incheieri()
  //     resp4 = status_change()
  //   })()

  //   console.log("ended");

  //   console.log(resp1)
  //   console.log(resp2)
  //   console.log(resp3)
  //   console.log(resp4)

  // })()
}
