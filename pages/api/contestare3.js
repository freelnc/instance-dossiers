const axios = require("axios");
var FormData = require('form-data');
export default async function  handler(req, res) {

    const { value } = req.query;
    let sidx = "data_inregistrare desc, data_inregistrare";
    let filters = `{"groupOp":"AND","rules":[{"field":"nr_inregistrare","op":"cn","data":${value}}]}`
    let result =  await getData("http://jurisprudenta.csj.md/grid_lista_dosare.php", filters,sidx);    


    let arr = []
    
    arr = await getDateFromAgenda(result,value)
 
    console.log(arr)
}


async function getDateFromAgenda(result,value){
    let arr = []
    let obj = {};

    for (const element of result.data.rows) {
        
            if(element.cell[1] === value) {
                obj.registerNumber = value
                obj = {}
                obj.rezultat = element.cell[9]
                obj.status = element.cell[4]
                obj.dataSedintei = ""
                arr[element.cell[3]] = obj
                let sidx = "data_sedinta desc, data_sedinta";
                let filters = `{"groupOp":"AND","rules":[{"field":"nr_dosar","op":"cn","data":"${element.cell[3]}"}]}`
                
                let date = await getData("http://agenda.csj.md/civil_grid.php",filters,sidx)
                
                if(date.data.rows != undefined)
                {
                arr[element.cell[3]].dataSedintei = date.data.rows[0].cell[4]
                
                
                }                                 
                }
                                        

             }
    
return arr
    
        }


async function getData(url,filters,sidx){
    var bodyFormData = new URLSearchParams();
    bodyFormData.append("_search", "true");
    
    bodyFormData.append("nd",  Date.now().toString());
    bodyFormData.append("rows", "50");
    bodyFormData.append("page", "1");
    bodyFormData.append("sidx", sidx);
    bodyFormData.append("sord", "desc");
    bodyFormData.append("filters", filters);
    const  result = await
     axios({
    method: "post",
    url: url,
    data: bodyFormData,
    // headers: {
    //     "Accept": "application/json, text/javascript, */*; q=0.01",
    //     "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    //     "Origin": "http://jurisprudenta.csj.md",
    //     "Referer": "http://jurisprudenta.csj.md/db_lista_dosare.php",
    //     "Cookie": "_ga=GA1.2.829236251.1620206427; _gid=GA1.2.477035096.1621232362",
    //     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.62",
    //     "Host": "jurisprudenta.csj.md"
    // }
  })
  return result;
}