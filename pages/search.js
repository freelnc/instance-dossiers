import clsx from 'clsx'
import SearchBar from '../components/SearchBar'
import {
  wrapper,
  flexCentered,
  mainTitle,
  subTitle,
  searchHint
} from '../styles/page.module.css'

export default function page() {
  return (
    <div className={clsx(wrapper, flexCentered)}>
      <div>
        <h1 className={mainTitle}>Găsește orice dosar</h1>
        <div className={subTitle}>Simplu și Rapid</div>
        <div style={{ marginTop: '6rem'}}>
          <SearchBar />
        </div>
        <div className={searchHint}>
          <span>Fiecare câmp necesită numărul dosarului de la etapa specificată.<br />Căutarea poate rula având cel puțin unul din câmpuri completate.</span>
        </div>
      </div>
    </div>
  )
}