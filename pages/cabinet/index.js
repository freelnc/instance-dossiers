import { useEffect } from 'react'
import { Button, Grid } from '@material-ui/core'
import axios from 'axios'
import redirectIfAuthenticated from '../../middleware/redirectIfAuthenticated'
import withToken from '../../utils/withToken'
import { makeStyles } from '@material-ui/core/styles';
import Navbar from '/components/Navbar'
import { getSession } from '/redux/sessionSlice'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import Loader from '/components/Loader'
import TaskRow from '/components/TaskRow'

const useStyles = makeStyles((theme) => ({
  wrapper: {
    minHeight: '100vh',
  },
  content: {
    padding: '24px',
  },
}));

const tasks = [
  {
    id: 1,
    name: 'First Task',
    service: {
      id: 1,
      name: "Service 1"
    },
    client: {
      id: 1,
      name: 'Unifun S.R.L.'
    },
    owner: {
      id: 1,
      name: 'Turuta Marin',
    },
    duedate: 'Tomorow',
  },
  {
    id: 1,
    name: 'First Task',
    service: {
      id: 1,
      name: "Service 1"
    },
    client: {
      id: 1,
      name: 'Unifun S.R.L.'
    },
    owner: {
      id: 1,
      name: 'Turuta Marin',
    },
    duedate: 'Tomorow',
  }
]

export default function handler() {
  const router = useRouter()
  const classes = useStyles()
  const dispatch = useDispatch()
  const session = useSelector(state => state.session)
  
  useEffect(() => {
    withToken()

    if (session.checked == false && session.status != 'loading') {
      dispatch(getSession())
    }
  }, [dispatch])

  if (session.status == 'pending' || session.checked == false) {
    return <Loader />
  }

  if (session.status == 'fulfilled' && session.checked == false && session.active == false) {
    router.push('/auth/login')
    return <></>
  }

  return (
    <Grid container direction="column" className={classes.wrapper}>
      <Grid item>
        <Navbar />
      </Grid>
      <Grid item className={classes.content}>
        <p>Wellcome home!</p>
        <div>
          {tasks.map((task) => (
            <TaskRow task={task} />
          ))}
        </div>
      </Grid>
    </Grid>
  )
}