
import { Provider } from 'react-redux';
import store from '/src/store';
import './main.css'
import '../styles/global.css'
import theme from '/src/theme'
import { ThemeProvider } from '@material-ui/core/styles'

export default function App({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />  
      </ThemeProvider>
    </Provider>
  )
}