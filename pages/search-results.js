import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import clsx from 'clsx'
import SearchBar from '../components/SearchBar'
import Dossier from '../components/Dossier'
import styles from '../styles/page.module.css'

const demoDossier = {
  "id":"51-4-1936-16052018",
  "name":"Frunze Mihail Vasile",
  "type":"Contravențional",
  "instance":"Judecătoria Ungheni",
  "judge":"Alexandru  Parfeni",
  "status":"",
  "timeline":[
    {
      "type":"meeting",
      "date":"14-06-2018",
      "details":{
        "date":"14-06-2018",
        "place":"Sala de ședințe nr.19.",
        "time":"10:00",
        "result":"Ședința  a avut loc, se recunoaște vinovat Frunze Mihail și se aplică o pedeapsă dub formă de amendă în mărime de 450 u.c ceea ce constituie 22500 lei"
      }
    },
    {
      "type":"decision",
      "date":"2018-06-15",
      "details":{
        "publicationDate":"2018-06-15",
        "pronunciationDate":"2018-06-14",
        "docLink":"pigd_integration/pdf/7D77E412-8270-E811-80D5-0050568B44C1"
      }
    },
    {
      "type":"status",
      "date":"2018-05-16",
      "details":{
        "status":"Arhivat"
      }
    }
  ]
}

export default function page() {
  const [dossier, setDossier] = useState(false)
  const [pending, setPending] = useState(false)
  const router = useRouter()
  // const { id1 } = router.query

  useEffect(async () => {
    if (pending == false && dossier == false && router.query.id) {
      setPending(true)
      const resp = await fetch(`/api/dossier?id=${router.query.id}`)
      if (resp.status == 200) {
        console.log(resp)
        const json = await resp.json()
        console.log(json)
        setDossier(json)
      } else {
        console.log(resp)
      }
    }
  })

  return (
    <div className={clsx(styles.wrapper, styles.flexColumn)}>
      <div className={styles.justifiedCenter}>
        <SearchBar />
      </div>
      <div>
        <div style={{marginTop: '100px'}}>
          { dossier ? <Dossier dossier={dossier} /> : '' }
        </div>
      </div>
    </div>
  )
}