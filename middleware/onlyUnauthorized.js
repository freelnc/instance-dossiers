import jwt from 'jsonwebtoken'

export default (handler) => {
  return async (req, res) => {
    try {
      const token = req.headers.authorization.match(/Bearer\s+(.*)/)[1]
      req.session = jwt.verify(token, process.env.JWT_SECRET)

      return handler(req, res)
    } catch (err) {
      res.status(200).json({ ok: false })
    }
  }
}
