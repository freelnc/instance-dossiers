import { configureStore } from '@reduxjs/toolkit'
import axios from 'axios'
import profileReducer from '/redux/profileSlice'
import sessionReducer from '/redux/sessionSlice'

axios.defaults.baseURL = 'http://localhost:3000/api';

export default configureStore({
  reducer: {
    profile: profileReducer,
    session: sessionReducer
  },
});
